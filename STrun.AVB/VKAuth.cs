﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace STrun.AVB
{
    class VKAuth
    {
        public VkApi VkPart;

        public void VkNetAuth(TextBox LoginTextBox, PasswordBox PasswordBox)
        {
            VkPart = new VkApi();

            VkPart.Authorize(new ApiAuthParams
            {
                ApplicationId = 6334921,
                Login = LoginTextBox.Text,
                Password = PasswordBox.Password,
                Settings = Settings.All,

                TwoFactorAuthorization = () =>
                {
                    //MessageBox.Show("Введите код в поле 'Код'", "", MessageBoxButton.OK);
                    return Console.ReadLine();
                }
            });
        }
    }
}
