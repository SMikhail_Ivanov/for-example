﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes;
using MaterialDesignColors;
using MaterialForms;
using MahApps;
using MaterialSkin;
using MaterialDesign;
using VkNet;

namespace STrun.AVB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow :  Window
    {
        // https://mahapps.com/guides/quick-start.html
        
        private Auth ForAuth = new Auth();
        private string SearchedUserName;
        private Int32 SearchedUserID;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void AuthButton_Click(object sender, RoutedEventArgs e)
        {
            ForAuth.Authorization(LoginTextBox, PasswordBox, AuthButton, LogOutButton);

            try
            {
                var UPDCommand = new MySqlCommand("Select LoginName from Users", ForAuth.CurrentConnection);

                MySqlDataReader R = UPDCommand.ExecuteReader();

                while (R.Read())
                {
                    PeopleListComboBox.Items.Add((R["LoginName"]));
                }
                R.Close();
            }
            catch (Exception Error)
            {
                MessageBox.Show(Error.Message);
            }
        }

        private void LogOutButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RefreshMessages()
        {
            /*try
            {
                var UPDCommand = new MySqlCommand("Select MessageText from Messages " +
                    " Where FirstUserID = " + ForAuth.CurrentUserId + " and SecondUserID = " + SearchedUserID, ForAuth.CurrentConnection);

                MySqlDataReader R = UPDCommand.ExecuteReader();

                while (R.Read())
                {
                    SelfListBox.Items.Add((R["MessageText"]));
                }
                R.Close();

            }
            catch (Exception Error)
            {
                MessageBox.Show(Error.Message);
            }*/

            try
            {
                MySqlCommand UPDCommand = new MySqlCommand();

                if (ForAuth.CurrentUserId != SearchedUserID)
                {
                    UPDCommand = new MySqlCommand("Select FirstUserID, LoginName, MessageText from Messages " +
                         "inner join Users on Messages.FirstUserID = Users.UserID Where FirstUserID <> SecondUserID" ,ForAuth.CurrentConnection);
                }
                else
                {
                    UPDCommand = new MySqlCommand("Select FirstUserID, LoginName, MessageText from Messages " +
                         "inner join Users on Messages.FirstUserID = Users.UserID Where FirstUserID = " + SearchedUserID + " and SecondUserID = " + ForAuth.CurrentUserId, ForAuth.CurrentConnection);
                }

                MySqlDataReader R = UPDCommand.ExecuteReader();

                while (R.Read())
                {
                    string 
                        Node,
                        MessageText,
                        Login;
                    int 
                        UserID;
                    Login = (string)R["LoginName"];
                    UserID = (Int32)(R["FirstUserID"]);
                    MessageText = (string)R["MessageText"];
                    Node = Login + ": " + MessageText; 
                    
                    PartnerListBox.Items.Add(Node);
                    PartnerListBox.Tag = UserID;
                }
                R.Close();
            }
            catch (Exception Error)
            {
                MessageBox.Show(Error.Message);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var UPDCommand = new MySqlCommand("Select UserID, LoginName from Users " +
                " Where LoginName = '" + SearchTextBox.Text + "'", ForAuth.CurrentConnection);

                MySqlDataReader R = UPDCommand.ExecuteReader();

                while (R.Read())
                {
                    SearchedUserID = (Int32)(R["UserID"]);
                    SearchedUserName = (string)(R["LoginName"]);
                }
                R.Close();

                UserNameFromSearchTextBlock.Text = "For: " + SearchedUserName;

                RefreshMessages();
                PartnerTextBlock.Text = SearchedUserName;
            }
            catch(Exception Error)
            {
                MessageBox.Show(Error.Message);
            }
        }

        private void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageTextTextBox.Text != "")
            {
                try
                {
                    var UPDCommand = new MySqlCommand("Insert into Messages(FirstUserID, SecondUserID, MessageText) Values(" + ForAuth.CurrentUserId + "," + SearchedUserID + ", '" + MessageTextTextBox.Text + "')", ForAuth.CurrentConnection);
                    UPDCommand.ExecuteScalar();
                }
                catch (Exception Error)
                {
                    MessageBox.Show(Error.Message);
                }
                MessageTextTextBox.Text = "";
                RefreshButton_Click(sender, e);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            //SelfListBox.Items.Clear();
            PartnerListBox.Items.Clear();

            RefreshMessages();
        }

        private void SearchComboBoxButton_Click(object sender, RoutedEventArgs e)
        {
            //SelfListBox.Items.Clear();
            PartnerListBox.Items.Clear();

            try
            {
                var UPDCommand = new MySqlCommand("Select UserID, LoginName from Users " +
                " Where LoginName = '" + PeopleListComboBox.Text + "'", ForAuth.CurrentConnection);

                MySqlDataReader R = UPDCommand.ExecuteReader();

                while (R.Read())
                {
                    SearchedUserID = (Int32)(R["UserID"]);
                    SearchedUserName = (string)(R["LoginName"]);
                }
                R.Close();

                UserNameFromSearchTextBlock.Text = "For: " + SearchedUserName;

                RefreshMessages();
                PartnerTextBlock.Text = SearchedUserName;
            }
            catch (Exception Error)
            {
                MessageBox.Show(Error.Message);
            }
        }

        private void LogOutButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                PeopleListComboBox.IsEnabled = true;
                SearchComboBoxButton.IsEnabled = true;
                SearchTextBox.IsEnabled = true;
                SearchButton.IsEnabled = true;
                SendMessageButton.IsEnabled = true;
                RefreshButton.IsEnabled = true;
                MessageTextTextBox.IsEnabled = true;
            }
            catch(Exception Error) { }
        }

        private void _2__Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Введите код в поле 'Код'", "", MessageBoxButton.OK);
        }
    }
}
